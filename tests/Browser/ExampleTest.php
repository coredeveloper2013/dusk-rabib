<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function ($browser) {
            $browser->visit('https://www.visualcomfort.com/trade-portal/')
                ->type('ctl00$ctl00$ctl00$ContentPlaceHolderDefault$Content$Login_3$TxtLogin', '12720CD')
                ->type('ctl00$ctl00$ctl00$ContentPlaceHolderDefault$Content$Login_3$TxtPassword', 'Success7')
                ->press('ctl00$ctl00$ctl00$ContentPlaceHolderDefault$Content$Login_3$BtnLogin')
                ->waitForText('Shop By')
                ->clickLink('Shop By')
                ->assertPathIs('/shop-by/')
                ->clickLink('Categories')
                ->assertPathIs('/categories/');

        });
    }
}
