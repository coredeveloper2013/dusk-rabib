<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RetriveInfo extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function ($browser) {
            $browser->visit('https://www.visualcomfort.com/trade-portal/')
                ->type('text', '12720CD')
                ->type('password', 'Success7')
                ->press('Login')
                ->assertPathIs('https://www.visualcomfort.com/shop-by/')
                ->waitForText('Designers');
        });
    }
}
