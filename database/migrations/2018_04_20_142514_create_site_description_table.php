<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_description', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id')->unique();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('sku')->nullable();
            $table->string('price')->nullable();
            $table->string('availability')->nullable();
            $table->string('backorder_status')->nullable();
            $table->string('image_url')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('weight')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_description');
    }
}
